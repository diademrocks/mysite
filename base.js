function view(src) {
  var viewStyle = document.getElementById('view-img').style;
  var fadeStyle = document.getElementById('fade').style;

  viewStyle.visibility = 'visible';
  viewStyle.opacity = '1';
  viewStyle.transform = 'translate(-50%, -50%)';


  fadeStyle.visibility = 'visible';
  fadeStyle.opacity = '0.6';

  document.getElementById('view-img').src = src;
}

function exit() {
  var viewStyle = document.getElementById('view-img').style;
  var fadeStyle = document.getElementById('fade').style;

  viewStyle.visibility = 'hidden';
  viewStyle.opacity = '0';


  fadeStyle.visibility = 'hidden';
  fadeStyle.opacity = '0';
}
